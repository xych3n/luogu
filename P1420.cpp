#include <bits/stdc++.h>
using namespace std;

int main(void)
{
    int n, a, b = -100;
    cin >> n;
    int ans = 1, count = 0;
    while (n--) {
        cin >> a;
        if (a == b + 1) {
            ++count;
        } else {
            ans = max(ans, count);
            count = 1;
        }
        b = a;
    }
    cout << max(ans, count) << endl;
    return 0;
}
