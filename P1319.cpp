#include <bits/stdc++.h>
using namespace std;

int main(void)
{
    int n, m, c = 0, j = 0;
    cin >> n;
    while (cin >> m) {
        while (m--) {
            cout << c;
            if (++j == n) {
                cout << endl;
                j = 0;
            }
        }
        c = !c;
    }
    return 0;
}
