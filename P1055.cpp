#include <bits/stdc++.h>
using namespace std;

int main(void)
{
    string s;
    cin >> s;
    int code = 0, w = 1;
    for (int i = 0; i < 11; ++i) {
        if (isdigit(s[i])) {
            code += w++ * (s[i] - '0');
        }
    }
    code %= 11;
    code = code == 10 ? 'X' : code + '0';
    if (s.back() == (char)code) {
        cout << "Right\n";
    } else {
        s.back() = (char)code;
        cout << s << endl;
    }
    return 0;
}
