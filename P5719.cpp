#include <bits/stdc++.h>
using namespace std;

int main(void)
{
    int n, k;
    cin >> n >> k;
    int m = n / k;
    cout << fixed << setprecision(1)
         << k * (1 + m) / 2.0 << ' '
         << (n * (n + 1) - k * m * (1 + m)) / (2.0 * (n - m)) << endl;
    return 0;
}
