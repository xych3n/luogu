#include <bits/stdc++.h>
using namespace std;

constexpr array<const char*, 14> ans = {
    "I love Luogu!",
    "6 4",
    "3\n12\n2",
    "166.667",
    "15",
    "10.8167",
    "110\n90\n0",
    "31.4159\n78.5398\n523.599",
    "22",
    "9",
    "33.3333",
    "13\nR",
    "16",
    "50"
};

int main(void)
{
    int T;
    cin >> T;
    cout << ans[T - 1] << endl;
    return 0;
}
