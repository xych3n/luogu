#include <bits/stdc++.h>
using namespace std;

int main(void) {
    int n, a[1001];
    cin >> n;
    for (int i = 0; i < n; ++i) {
        cin >> a[i];
    }
    cout << fixed << setprecision(2)
         << (double)(accumulate(a, a + n, 0) - *min_element(a, a + n) - *max_element(a, a + n)) / (n - 2) << endl;
    return 0;
}
