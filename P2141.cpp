#include <bits/stdc++.h>
using namespace std;

int main(void)
{
    int n, num[101] = {0};
    char sum[20001] = {0};
    cin >> n;
    for (int i = 0; i < n; ++i) {
        cin >> num[i];
    }
    for (int i = 1; i < n; ++i) {
        for (int j = 0; j < i; ++j) {
            sum[num[i] + num[j]] = 1;
        }
    }
    int ans = 0;
    for (int i = 0; i < n; ++i) {
        if (sum[num[i]]) {
            ans++;
        }
    }
    cout << ans << endl;
    return 0;
}
