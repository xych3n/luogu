#include <bits/stdc++.h>
using namespace std;

int main(void)
{
    int n;
    cin >> n;
    cout << (5 * n < 3 * n + 11 ? "Local" : "Luogu") << endl;
    return 0;
}
