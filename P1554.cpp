#include <bits/stdc++.h>
using namespace std;

int main(void)
{
    int M, N, count[10] = {0};
    cin >> M >> N;
    while (M <= N) {
        string s = to_string(M);
        for (const auto& c: s) {
            count[c - '0']++;
        }
        ++M;
    }
    for (int i = 0; i < 10; ++i) {
        cout << count[i] << ' ';
    }
    cout << endl;
    return 0;
}
