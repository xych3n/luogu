#include <bits/stdc++.h>
using namespace std;

int main(void)
{
    int n;
    cin >> n;
    int x = 1;
    for (int i = 0; i < n; ++i) {
        for (int j = 0; i + j < n; ++j) {
            cout << setw(2) << setfill('0') << x++;
        }
        cout << endl;
    }
    return 0;
}
