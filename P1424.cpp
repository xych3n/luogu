#include <bits/stdc++.h>
using namespace std;

int main(void)
{
    int x, n;
    cin >> x >> n;
    cout << 250 * ((n + x - 1) / 7 * 5 - min(x - 1, 5) +
                   min((n + x - 1) % 7, 5)) << endl;
    // cout << 250 * (n - (n + x - 1) / 7 - (n + x) / 7 + x / 7) << endl;
    return 0;
}
