#include <bits/stdc++.h>
using namespace std;

int main(void)
{
    int n, i, number, price, spend, ans = INT_MAX;
    cin >> n;
    for (i = 0; i < 3; ++i) {
        cin >> number >> price;
        spend = (n + number - 1) / number * price;
        if (spend < ans) ans = spend;
    }
    cout << ans << endl;
    return 0;
}
