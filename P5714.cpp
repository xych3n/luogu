#include <bits/stdc++.h>
using namespace std;

int main(void)
{
    double m, h;
    cin >> m >> h;
    m /= h * h;
    if (m < 18.5) {
        cout << "Underweight\n";
    } else if (m < 24) {
        cout << "Normal\n";
    } else {
        cout << m << "\nOverweight\n";
    }
    return 0;
}
