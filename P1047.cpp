/* Time: O(mlogm), Space: O(m) */
#include <bits/stdc++.h>
using namespace std;

int main(void)
{
    int l, m, u, v, i, ans;
    vector<pair<int, int>> ranges;
    cin >> l >> m;
    for (i = 0; i < m; ++i) {
        cin >> u >> v;
        ranges.emplace_back(u, v);
    }
    sort(ranges.begin(), ranges.end());
    ans = l + 1;
    for (i = 0; i < m; ++i) {
        if (i > 0 && ranges[i].first <= ranges[i - 1].second) {
            ranges[i].first = ranges[i - 1].second + 1;
            if (ranges[i].first > ranges[i].second) {
                continue;
            }
        }
        ans -= ranges[i].second - ranges[i].first + 1;
    }
    cout << ans << endl;
    return 0;
}

/* Time: O(lm), Space: O(l) */
// #include <bits/stdc++.h>
// using namespace std;

// int main(void)
// {
//     bool moved[10001] = {0};
//     int l, m, u, v, i, j, ans = 0;
//     cin >> l >> m;
//     for (i = 0; i < m; ++i) {
//         cin >> u >> v;
//         for (j = u; j <= v; ++j) {
//             moved[j] = true;
//         }
//     }
//     for (j = 0; j <= l; ++j) {
//         if (!moved[j]) {
//             ++ans;
//         }
//     }
//     cout << ans << endl;
//     return 0;
// }
