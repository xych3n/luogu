#include <bits/stdc++.h>
using namespace std;

template <typename _Tp>
class Graph
{
public:
    typedef size_t size_type;
    typedef _Tp weight_type;

private:
    const size_type _n;
    vector<vector<pair<size_type, weight_type>>> _adj_list;

public:
    Graph(size_type n): _n(n), _adj_list(n) {}

    void addEdge(size_type u, size_type v, weight_type w)
    {
        _adj_list[u].emplace_back(v, w);
        // _adj_list[v].emplace_back(u, w);
    }

    vector<weight_type> dijkstra(size_type s)
    {
        vector<weight_type> dist(_n, numeric_limits<weight_type>::max());
        dist[s] = 0;
        priority_queue<pair<size_type, weight_type>, vector<pair<size_type, weight_type>>,
            greater<pair<size_type, weight_type>>> pq;
        pq.emplace(0, s);
        while (!pq.empty()) {
            auto u = pq.top().second;
            auto d = pq.top().first;
            pq.pop();
            if (d > dist[u]) {
                continue;
            }
            // for (const auto &[v, w] : _adj_list[u]) {
            for (const auto &edge: _adj_list[u]) {
                auto &v = edge.first;
                auto &w = edge.second;
                if (dist[u] + w < dist[v]) {
                    dist[v] = dist[u] + w;
                    pq.emplace(dist[v], v);
                }
            }
        }
        return dist;
    }
};

int main(void)
{
    int n, m, s;
    cin >> n >> m >> s;
    Graph<int> g(n);
    for (int i = 0; i < m; ++i) {
        int u, v, w;
        cin >> u >> v >> w;
        g.addEdge(u - 1, v - 1, w);
    }
    auto dist = g.dijkstra(s - 1);
    for (int i = 0; i < n; ++i) {
        cout << dist[i] << ' ';
    }
    cout << endl;
    return 0;
}
