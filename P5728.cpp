#include <bits/stdc++.h>
using namespace std;

int main(void)
{
    int N, score[1001][4];
    cin >> N;
    int ans = 0;
    for (int i = 0; i < N; ++i) {
        cin >> score[i][0] >> score[i][1] >> score[i][2];
        score[i][3] = score[i][0] + score[i][1] + score[i][2];
        for (int j = 0; j < i; ++j) {
            if (abs(score[i][3] - score[j][3]) <= 10 &&
                abs(score[i][0] - score[j][0]) <= 5 &&
                abs(score[i][1] - score[j][1]) <= 5 &&
                abs(score[i][2] - score[j][2]) <= 5
            ) {
                ++ans;    
            }
        }
    }
    cout << ans << endl;
    return 0;
}
