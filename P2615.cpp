#include <bits/stdc++.h>
using namespace std;

int main(void)
{
    short N, i, j, a[39][39] = {0};
    cin >> N;
    i = 0, j = N / 2;
    a[i][j] = 1;
    for (short K = 2; K <= N * N; ++K) {
        if (i == 0) {
            if (j == N - 1) {
                i++;
            } else {
                i = N - 1, j++;
            }
        } else {
            if (j == N - 1) {
                i--, j = 0;
            } else {
                if (a[i - 1][j + 1] == 0) {
                    i--, j++;
                } else {
                    i++;
                }
            }
        }
        a[i][j] = K;
    }
    for (i = 0; i < N; ++i) {
        for (j = 0; j < N; ++j) {
            cout << a[i][j] << ' ';
        }
        cout << endl;
    }
    return 0;
}
