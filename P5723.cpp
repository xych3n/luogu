#include <bits/stdc++.h>
using namespace std;

bool is_prime(int n)
{
    for (int i = 3; i * i <= n; ++i) {
        if (n % i == 0) {
            return false;
        }
    }
    return true;
}

int main(void)
{
    int L;
    cin >> L;
    if (L == 1) {
        cout << "0\n";
    } else if (L < 5) {
        cout << "2\n1\n";
    } else {
        int sum = 2, count = 1;
        cout << 2 << endl;
        for (int i = 3; sum + i <= L; i += 2) {
            if (is_prime(i)) {
                sum += i;
                ++count;
                cout << i << endl;
            }
        }
        cout << count << endl;
    }
    return 0;
}
