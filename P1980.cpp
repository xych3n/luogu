#include <bits/stdc++.h>
using namespace std;

int main(void)
{
    int n;
    char x;
    cin >> n >> x;
    int ans = 0;
    for (int i = 1; i <= n; ++i) {
        string s = to_string(i);
        for (const auto& c: s) {
            if (c == x) {
                ++ans;
            }
        }
    }
    cout << ans << endl;
    return 0;
}
