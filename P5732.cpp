#include <bits/stdc++.h>
using namespace std;

int main(void)
{
    int n;
    cin >> n;
    for (int i = 0; i < n; ++i) {
        int k = 1;
        for (int j = 0; j <= i; ++j) {
            cout << k << ' ';
            k = k * (i - j) / (j + 1);
        }
        cout << endl;
    }
    return 0;
}

// #include <bits/stdc++.h>
// using namespace std;

// int main(void)
// {
//     int n, a[2][20];
//     cin >> n;
//     if (n == 0) {
//         return 0;
//     }
//     cout << "1\n";
//     for (int i = 1; i < n; ++i) {
//         cout << (a[i&1][0] = 1) << ' ';
//         for (int j = 1; j < i; ++j) {
//             cout << (a[i&1][j] = a[!(i&1)][j] + a[!(i&1)][j - 1]) << ' ';
//         }
//         cout << (a[i&1][i] = 1) << '\n';
//     }
//     return 0;
// }
