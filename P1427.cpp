#include <bits/stdc++.h>
using namespace std;

int main(void)
{
    int a[100], n = -1;
    do {
        cin >> a[++n];
    } while (a[n] != 0);
    while (n--) {
        cout << a[n] << ' ';
    }
    cout << endl;
    return 0;
}
