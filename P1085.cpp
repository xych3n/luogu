#include <bits/stdc++.h>
using namespace std;

int main(void)
{
    int a, b, i, val = -1, ind;
    for (i = 1; i <= 7; ++i) {
        cin >> a >> b;
        a += b;
        if (a > val) {
            val = a, ind = i;
        }
    }
    if (val > 8) {
        cout << ind << endl;
    } else {
        cout << "0\n";
    }
    return 0;
}
