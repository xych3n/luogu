#include <bits/stdc++.h>
using namespace std;

int main(void)
{
    int x;
    cin >> x;
    cout << "Today, I ate " << x << " apple" << (x > 1 ? "s" : "") << ".\n";
    return 0;
}
