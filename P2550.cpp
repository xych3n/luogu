#include <bits/stdc++.h>
using namespace std;

int main(void)
{
    short n, t, winner[8] = {0};
    char table[33] = {0};
    cin >> n;
    for (int i = 0; i < 7; ++i) {
        cin >> t;
        table[t - 1] = 1;
    }
    while (n--) {
        int count = 0;
        for (int i = 0; i < 7; ++i) {
            cin >> t;
            if (table[t - 1]) {
                ++count;
            }
        }
        winner[count]++;
    }
    for (int i = 7; i > 0; --i) {
        cout << (int)winner[i] << ' ';
    }
    cout << endl;
    return 0;
}
