#include <bits/stdc++.h>
using namespace std;

int main(void)
{
    int s1, s2, s3, a[81] = {0}, i, j, k;
    cin >> s1 >> s2 >> s3;
    for (i = 1; i <= s1; ++i) {
        for (j = 1; j <= s2; ++j) {
            for (k = 1; k <= s3; ++k) {
                a[i + j + k]++;
            }
        }
    }
    cout << max_element(a, a + s1 + s2 + s3) - a << endl;
    return 0;
}
