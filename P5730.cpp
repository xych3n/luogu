// WA on C++14 (GCC9)
#include <bits/stdc++.h>
using namespace std;

constexpr array<char[5][4], 10> table = {
    "XXX", "X.X", "X.X", "X.X", "XXX",
    "..X", "..X", "..X", "..X", "..X",
    "XXX", "..X", "XXX", "X..", "XXX",
    "XXX", "..X", "XXX", "..X", "XXX",
    "X.X", "X.X", "XXX", "..X", "..X",
    "XXX", "X..", "XXX", "..X", "XXX",
    "XXX", "X..", "XXX", "X.X", "XXX",
    "XXX", "..X", "..X", "..X", "..X",
    "XXX", "X.X", "XXX", "X.X", "XXX",
    "XXX", "X.X", "XXX", "..X", "XXX"
};

int main(void)
{
    int n;
    cin >> n;
    string s;
    cin >> s;
    for (int i = 0; i < 5; ++i) {
        for (int j = 0; j < n; ++j) {
            cout << table[s[j] - '0'][i] << (j == n - 1 ? '\n' : '.');
        }
    }
    return 0;
}
