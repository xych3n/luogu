#include <bits/stdc++.h>
using namespace std;

char a[100][100] = {0};

int main(void)
{
    int n, m, k, x, y;
    cin >> n >> m >> k;
    while (m--) {
        cin >> x >> y;
        --x, --y;
        for (int i = max(0, x - 2); i < n && i <= x + 2; ++i) {
            for (int j = max(0, y - 2); j < n && j <= y + 2; ++j) {
                if (abs(i - x) + abs(j - y) <= 2) {
                    a[i][j] = 1;
                }
            }
        }
    }
    while (k--) {
        cin >> x >> y;
        --x, --y;
        for (int i = max(0, x - 2); i < n && i <= x + 2; ++i) {
            for (int j = max(0, y - 2); j < n && j <= y + 2; ++j) {
                a[i][j] = 1;
            }
        }
    }
    int ans = 0;
    for (int i = 0; i < n; ++i) {
        ans += count(a[i], a[i] + n, 0);
    }
    cout << ans << endl;
    return 0;
}
