#include <bits/stdc++.h>
using namespace std;

int main(void)
{
    string s;
    cin >> s;
    if (s[0] == '-') {
        cout << '-';
        s.erase(0, 1);
    }
    reverse(s.begin(), s.end());
    auto it = find_if(s.begin(), s.end(), [](char c) { return c != '0'; });
    if (it == s.end()) {
        cout << "0\n";
    } else {
        cout << string(it, s.end()) << endl;
    }
    return 0;
}
