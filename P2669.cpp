#include <bits/stdc++.h>
using namespace std;

int main(void)
{
    int k;
    cin >> k;
    int sum = 0, coin = 1, days = 1;
    for (int i = 0; i < k; ++i) {
        if (days == 0) {
            days = ++coin;
        }
        sum += coin;
        --days;
    }
    cout << sum << endl;
    return 0;
}
