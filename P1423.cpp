#include <bits/stdc++.h>
using namespace std;

int main(void)
{
    double s;
    cin >> s;
    double d = 2.0;
    int ans = 0;
    while (s > 0) {
        s -= d;
        d *= 0.98;
        ++ans;
    }
    cout << ans << endl;
    return 0;
}
