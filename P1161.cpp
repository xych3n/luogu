#include <bits/stdc++.h>
using namespace std;

int main(void)
{
    int n, t;
    double a;
    bool b[2000001] = {0};
    cin >> n;
    for (int i = 0; i < n; ++i) {
        cin >> a >> t;
        for (int j = 1; j <= t; ++j) {
            b[(int)(j * a)] = !b[(int)(j * a)];
        }
    }
    cout << find_if(b, b + 2000001, [](bool x) { return x; }) - b << endl;
    return 0;
}
