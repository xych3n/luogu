#include <bits/stdc++.h>
using namespace std;

int main(void)
{
    int a[3], legsq, hypsq;
    cin >> a[0] >> a[1] >> a[2];
    sort(a, a + 3);
    if (a[0] + a[1] <= a[2]) {
        cout << "Not triangle\n";
    } else {
        legsq = a[0] * a[0] + a[1] * a[1];
        hypsq = a[2] * a[2];
        if (legsq == hypsq) {
            cout << "Right triangle\n";
        } else if (legsq > hypsq) {
            cout << "Acute triangle\n";
        } else {
            cout << "Obtuse triangle\n";
        }
        if (a[0] == a[1] || a[1] == a[2]) {
            cout << "Isosceles triangle\n";
            if (a[0] == a[2]) {
                cout << "Equilateral triangle\n";
            }
        }
    }
    return 0;
}
