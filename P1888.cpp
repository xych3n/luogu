#include <bits/stdc++.h>
using namespace std;

int main(void)
{
    int a[3];
    cin >> a[0] >> a[1] >> a[2];
    sort(a, a + 3);
    int d = __gcd(a[0], a[2]);
    a[0] /= d, a[2] /= d;
    cout << a[0] << '/' << a[2] << endl;
    return 0;
}
