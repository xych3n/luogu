#include <bits/stdc++.h>
using namespace std;

int main(void)
{
    char cube[21][21][21] = {0};
    int w, x, h, q;
    cin >> w >> x >> h >> q;
    while (q--) {
        int x1, y1, z1, x2, y2, z2;
        cin >> x1 >> y1 >> z1 >> x2 >> y2 >> z2;
        for (int i = x1 - 1; i < x2; ++i) {
            for (int j = y1 - 1; j < y2; ++j) {
                for (int k = z1 - 1; k < z2; ++k) {
                    cube[i][j][k] = 1;
                }
            }
        }
    }
    int ans = w * x * h;
    for (int i = 0; i < w; ++i) {
        for (int j = 0; j < x; ++j) {
            for (int k = 0; k < h; ++k) {
                if (cube[i][j][k]) {
                    --ans;
                }
            }
        }
    }
    cout << ans << endl;
    return 0;
}
