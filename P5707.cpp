#include <bits/stdc++.h>
using namespace std;

int main(void)
{
    int s, v;
    cin >> s >> v;
    int minutes = ceil(static_cast<double>(s) / v) + 10;
    if (minutes >= 24 * 60) {
        cout << "08:00\n";
    } else {
        int hours = minutes / 60;
        minutes %= 60;
        cout << setfill('0') << setw(2) << (7 - hours + 24) % 24 << ':'
             << setfill('0') << setw(2) << (60 - minutes) << endl;
    }
    return 0;
}
