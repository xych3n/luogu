#include <bits/stdc++.h>
using namespace std;

int main(void)
{
    string s;
    cin >> s;
    transform(s.begin(), s.end(), s.begin(),
        [](char c) { return isalpha(c) ? toupper(c) : c; });
    cout << s << endl;
    return 0;
}
