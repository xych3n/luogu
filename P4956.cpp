#include <bits/stdc++.h>
using namespace std;

int main(void)
{
    int n;
    cin >> n;
    n /= 364;
    int k = max(1, (n - 98) / 3);
    cout << n - 3 * k << endl << k << endl;
    return 0;
}
