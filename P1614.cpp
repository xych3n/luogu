#include <bits/stdc++.h>
using namespace std;

int main(void)
{
    int n, m, a[3001], i;
    cin >> n >> m;
    for (i = 0; i < n; ++i) {
        cin >> a[i];
    }
    int ans = 0, sum = 0;
    for (i = 0; i < m; ++i) {
        sum += a[i];
    }
    ans = sum;
    for (i = m; i < n; ++i) {
        sum += a[i] - a[i - m];
        ans = min(ans, sum);
    }
    cout << ans << endl;
    return 0;
}
