#include <bits/stdc++.h>
using namespace std;

int main(void)
{
    int money = 0, save = 0, budget, flag = 1;
    for (int X = 1; X <= 12; ++X) {
        cin >> budget;
        if (flag) {
            money += 300 - budget;
            if (money < 0) {
                cout << '-' << X << endl;
                flag = 0;
            } else {
                save += money / 100 * 100;
                money %= 100;
            }
        }
    }
    if (flag) {
        cout << money + save / 5 * 6 << endl;
    }
    return 0;
}
