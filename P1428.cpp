#include <bits/stdc++.h>
using namespace std;

int main(void)
{
    int n, a[101];
    cin >> n;
    for (int i = 0; i < n; ++i) {
        cin >> a[i];
        int count = 0;
        for (int j = 0; j < i; ++j) {
            if (a[j] < a[i]) {
                ++count;
            }
        }
        cout << count << ' ';
    }
    cout << endl;
    return 0;
}
