#include <bits/stdc++.h>
using namespace std;

int main(void)
{
    int n, a[119], size = 0;
    cin >> n;
    while (n != 1) {
        a[size++] = n;
        n = n % 2 ? 3 * n + 1 : n / 2;
    }
    cout << "1 ";
    while (size--) {
        cout << a[size] << ' ';
    }
    cout << endl;
    return 0;
}
