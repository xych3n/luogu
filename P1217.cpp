#include <bits/stdc++.h>
using namespace std;

bool is_palindrome(int n)
{
    int r = 0;
    while (r < n) {
        r = r * 10 + n % 10;
        n /= 10;
    }
    return n == r || n == r / 10;
}

bool is_prime(int n)
{
    if (n < 2) return false;
    if (n < 4) return true;
    if (n % 2 == 0 || n % 3 == 0) {
        return false;
    }
    for (int i = 5; i * i <= n; i += 6) {
        if (n % i == 0 || n % (i + 2) == 0) {
            return false;
        }
    }
    return true;
}

int main(void)
{
    int a, b;
    cin >> a >> b;
    for (; a <= b; ++a) {
        if (is_palindrome(a) && is_prime(a)) {
            cout << a << endl;
        }
    }
    return 0;
}
