#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void rot90(char *a, int n)
{
    char *t = malloc(n * n);
    int i, j;
    for (i = 0; i < n; ++i) {
        for (j = 0; j < n; ++j) {
            t[i * n + j] = a[(n - j - 1) * n + i];
        }
    }
    memcpy(a, t, n * n);
    free(t);
}

void hflip(char *a, int n)
{
    int i, j;
    char t;
    for (i = 0; i < n; ++i) {
        for (j = 0; j < n / 2; ++j) {
            t = a[i * n + j];
            a[i * n + j] = a[i * n + n - j - 1];
            a[i * n + n - j - 1] = t;
        }
    }
}

int solve(char *a, char *b, int n)
{
    int i, j;
    for (i = 1; i <= 3; ++i) {
        rot90(a, n);
        if (!memcmp(a, b, n * n * sizeof(char))) {
            return i;
        }
    }
    rot90(a, n);
    hflip(a, n);
    if (!memcmp(a, b, n * n * sizeof(char))) {
        return 4;
    }
    for (i = 0; i < 3; ++i) {
        rot90(a, n);
        if (!memcmp(a, b, n * n * sizeof(char))) {
            return 5;
        }
    }
    rot90(a, n);
    hflip(a, n);
    return memcmp(a, b, n * n * sizeof(char)) ? 7 : 6;
}

int main(void)
{
    int n, i;
    char *a, *b, t[11];
    scanf("%d", &n);
    a = malloc(n * n);
    b = malloc(n * n);
    for (i = 0; i < n; ++i) {
        scanf("%s", t);
        memcpy(a + i * n, t, n);
    }
    for (i = 0; i < n; ++i) {
        scanf("%s", t);
        memcpy(b + i * n, t, n);
    }
    printf("%d\n", solve(a, b, n));
    free(a);
    free(b);
    return 0;
}
