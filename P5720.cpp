#include <bits/stdc++.h>
using namespace std;

int main(void)
{
    int a;
    cin >> a;
    cout << (a == 1 ? 1 : (int)ceil(log2(a))) << endl;
    return 0;
}
