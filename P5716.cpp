#include <bits/stdc++.h>
using namespace std;

constexpr array<int, 12> days = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

int main(void)
{
    int y, m;
    cin >> y >> m;
    if (m != 2) {
        cout << days[m - 1] << endl;
    } else if (y % 400 == 0 || (y % 4 == 0 && y % 100 != 0)) {
        cout << "29\n";
    } else {
        cout << "28\n";
    }
    return 0;
}
