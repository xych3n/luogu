#include <bits/stdc++.h>
using namespace std;

int main(void)
{
    int h, r;
    cin >> h >> r;
    cout << ceil(20 / (3.14 * r * r * h) * 1000) << endl;
    return 0;
}
