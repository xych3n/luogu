#include <bits/stdc++.h>
using namespace std;

int main(void)
{
    int kWh;
    cin >> kWh;
    double cost = min(150, kWh) * 0.4463 +
                  min(250, max(0, kWh - 150)) * 0.4663 +
                  max(0, kWh - 400) * 0.5663;
    cout << fixed << setprecision(1) << cost << endl;
    return 0;
}
