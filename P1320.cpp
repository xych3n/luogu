#include <bits/stdc++.h>
using namespace std;

int main(void)
{
    string s, t;
    while (cin >> t) {
        s += t;
    }
    cout << sqrt(s.size()) << ' ';
    auto first = s.begin();
    if (*first != '0') {
        cout << "0 ";
    }
    while (first != s.end()) {
        auto last = find_if(first, s.end(), [&](auto c) { return c != *first; });
        cout << last - first << ' ';
        first = last;
    }
    cout << endl;
    return 0;
}
