#include <bits/stdc++.h>
using namespace std;

int main(void)
{
    int k, n;
    cin >> k;
    double s = 0;
    for (n = 1; s <= k; ++n) {
        s += 1.0 / n;
    }
    cout << n - 1 << endl;
    return 0;
}
