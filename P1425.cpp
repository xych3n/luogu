#include <bits/stdc++.h>
using namespace std;

int main(void)
{
    int a, b, c, d;
    cin >> a >> b >> c >> d;
    int minutes = (c - a) * 60 + d - b;
    cout << minutes / 60 << ' ' << minutes % 60 << endl;
    return 0;
}
