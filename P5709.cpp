#include <bits/stdc++.h>
using namespace std;

int main(void)
{
    int m, t, s;
    cin >> m >> t >> s;
    cout << (t ? max(0, m - (s + t - 1) / t) : 0) << endl;
    return 0;
}
