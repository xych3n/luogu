#include <bits/stdc++.h>
using namespace std;

int main(void)
{
    int x;
    cin >> x;
    bool a = x % 2 == 0, b = x > 4 && x <= 12;
    cout << (a && b) << ' ' << (a || b) << ' '
         << ((a || b) && (a ^ b)) << ' ' << !(a || b) << endl;
    return 0;
}
