#include <bits/stdc++.h>
using namespace std;

int main(void)
{
    int n, a[9][9] = {0}, i, j, x = 1;
    cin >> n;
    for (int l = 0; l < (n + 1) / 2; ++l) {
        for (j = l; j < n - l; ++j) {
            a[l][j] = x++;
        }
        for (i = l + 1; i < n - l; ++i) {
            a[i][n - l - 1] = x++;
        }
        for (j = n - l - 2; j >= l; --j) {
            a[n - l - 1][j] = x++;
        }
        for (i = n - l - 2; i > l; --i) {
            a[i][l] = x++;
        }
    }
    for (i = 0; i < n; ++i) {
        for (j = 0; j < n; ++j) {
            cout << setw(3) << a[i][j];
        }
        cout << endl;
    }
    return 0;
}
