#include <bits/stdc++.h>
using namespace std;

int main(void)
{
    int n;
    cin >> n;
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            cout << setw(2) << setfill('0')
                 << i * n + j + 1;
        }
        cout << endl;
    }
    cout << endl;
    for (int i = 1; i <= n; ++i) {
        cout << string((n - i) * 2, ' ');
        for (int j = 1; j <= i; ++j) {
            cout << setw(2) << setfill('0')
                 << j + (i - 1) * i / 2;
        }
        cout << endl;
    }
    return 0;
}
